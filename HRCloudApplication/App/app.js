﻿var myApp = angular.module('myApp', ['ngRoute']);

myApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/contacts/', {
            controller: 'contactsController',
            templateUrl: '/App/views/contact/contacts.html'
        })
        .when('/contacts/create/', {
            controller: 'contactsController',
            templateUrl: '/App/views/contact/createContact.html'
        })
        .when('/contacts/create/:query', {
            controller: 'contactsController',
            templateUrl: '/App/views/contact/createContact.html'
        })
        .when('/contacts/edit/:idContact/', {
            controller: 'contactsController',
            templateUrl: '/App/views/contact/editContact.html'
        })
        .when('/contacts/edit/:idContact/:query', {
            controller: 'contactsController',
            templateUrl: '/App/views/contact/editContact.html'
        })
        .when('/contacts/details/:idContact/', {
            controller: 'contactsController',
            templateUrl: '/App/views/contact/detailsContact.html'
        })
        .when('/contacts/details/:idContact/:query', {
            controller: 'contactsController',
            templateUrl: '/App/views/contact/detailsContact.html'
        })
        .when('/contacts/delete/:idContact/', {
            controller: 'contactsController',
            templateUrl: '/App/views/contact/deleteContact.html'
        })
        .when('/contacts/delete/:idContact/:query', {
            controller: 'contactsController',
            templateUrl: '/App/views/contact/deleteContact.html'
        })
        .when('/emails/:idContact/:name', {
            controller: 'emailsController',
            templateUrl: '/App/views/email/emails.html'
        })
        .when('/emails/create/:idContact/:name', {
            controller: 'emailsController',
            templateUrl: '/App/views/email/createEmail.html'
        })
        .when('/emails/create/:idContact/:name/:query', {
            controller: 'emailsController',
            templateUrl: '/App/views/email/createEmail.html'
        })
        .when('/emails/:idContact/:name/:query', {
            controller: 'emailsController',
            templateUrl: '/App/views/email/emails.html'
        })
        .when('/emails/create/:idContact/:name/:query', {
            controller: 'emailsController',
            templateUrl: '/App/views/email/createEmail.html'
        })
        .when('/phones/:idContact/:name', {
            controller: 'phonesController',
            templateUrl: '/App/views/phone/phones.html'
        })
        .when('/phones/create/:idContact/:name', {
            controller: 'phonesController',
            templateUrl: '/App/views/phone/createPhone.html'
        })
        .when('/phones/:idContact/:name/:query', {
            controller: 'phonesController',
            templateUrl: '/App/views/phone/phones.html'
        })
        .when('/phones/create/:idContact/:name/:query', {
            controller: 'phonesController',
            templateUrl: '/App/views/phone/createPhone.html'
        })
        .when('/tags/:idContact/:name', {
            controller: 'tagsController',
            templateUrl: '/App/views/tag/tags.html'
        })
        .when('/tags/create/:idContact/:name', {
            controller: 'tagsController',
            templateUrl: '/App/views/tag/createTag.html'
        })
        .when('/tags/:idContact/:name/:query', {
            controller: 'tagsController',
            templateUrl: '/App/views/tag/tags.html'
        })
        .when('/tags/create/:idContact/:name/:query', {
            controller: 'tagsController',
            templateUrl: '/App/views/tag/createTag.html'
        })
        .when('/search/:query', {
            controller: 'searchController',
            templateUrl: '/App/views/contact/contacts.html'
        })
        .otherwise({
            redirectTo: '/contacts/'
        });
}]);


myApp.directive('tooltip', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element).hover(function () {
                // on mouseenter
                $(element).tooltip('show');
            }, function () {
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});