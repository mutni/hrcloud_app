﻿myApp.controller('contactsController', ['$scope', '$routeParams', 'contactsService', '$location', function ($scope, $routeParams, contactsService, $location) {

    if ($routeParams.query == null || $routeParams.query == "") {
        $scope.back = "/contacts/";
    }
    else {
        $scope.back = "/search/" + $routeParams.query;
    }

    $scope.getContacts = function () {
        contactsService.getContacts()
            .success(function (data) {
                $scope.contacts = data;
            })
            .error(function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
                console.log($scope.status);
            });
    }
    if ($location.url() == "/contacts/") {
        $scope.getContacts();
    }

    if ($routeParams.idContact != '' && $routeParams.idContact != null) {
        contactsService.getDetails($routeParams.idContact)
            .success(function (data) {
                $scope.details = data;
                $scope.query = $routeParams.query;
                if ($scope.query == null || $scope.query == "") {
                    $scope.back = "/contacts/";
                }
                else {
                    $scope.back = "/search/" + $scope.query;
                }
            })
            .error(function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
                console.log($scope.status);
            });
    }

    $scope.create = function (name, surname, address, email, phone, tags, a) {
        contactsService.create(name, surname, address, email, phone, tags)
            .success(function (data) {
                if (data == 'True') {
                    $location.url('/contacts/');
                }
                else {
                    confirm("All fields are required!");
                }
            })
    };

    $scope.edit = function (id, name, surname, address) {
        contactsService.edit(id, name, surname, address)
            .success(function (data) {
                if (data == 'True')
                    confirm("Contact updated");
                else {
                    confirm("Unable to update contact. All fields are required!");
                }
            })
    };

    $scope.delete = function (id, back) {
        contactsService.delete(id)
            .success(function (data) {
                if (data == 'True') {
                    $location.url(back);
                }
                else {
                    confirm("Error: cannot delete contact!");
                }
            })
    };

    $scope.action = function (location) {
        var a = '#/search/' + location
        window.location.replace(a);
    };
}]);