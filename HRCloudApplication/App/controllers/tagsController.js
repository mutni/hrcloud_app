﻿myApp.controller('tagsController', ['$scope', '$routeParams', 'tagsService', '$location', function ($scope, $routeParams, tagsService, $location) {

    $scope.getTags = function (id) {
        tagsService.getTags(id)
            .success(function (data) {
                $scope.tags = data;
                $scope.idContact = $routeParams.idContact;
                $scope.name = $routeParams.name;

            })
            .error(function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
                console.log($scope.status);
            });
    }
    if ($routeParams.idContact != "" && $routeParams.idContact != null) {
        $scope.getTags($routeParams.idContact);
    }
    if ($routeParams.query != "" && $routeParams.query != null) {
        $scope.query = $routeParams.query;
    }

    $scope.delete = function (idTag, idContact) {
        tagsService.delete(idTag, idContact)
            .success(function (data) {
                if (data == 'True') {
                    $scope.getTags($routeParams.idContact);
                }
                else {
                    confirm("Cannot delete tag. At least one tag is required!");
                }
            })
    };

    if ($routeParams.idContact != "" && $routeParams.idContact != null & $routeParams.name != "" && $routeParams.name != null) {
        $scope.idContact = $routeParams.idContact;
        $scope.name = $routeParams.name;
    }

    $scope.addTag = function (idContact, tag) {
        tagsService.addTag(idContact, tag)
            .success(function (data) {
                if (data == 'True') {
                    $location.url('/tags/' + $routeParams.idContact + '/' + $routeParams.name);
                }
                else
                    confirm("Error: cannon add tag!");
            })
    };

    $scope.action = function (location) {
        var a = '#/search/' + location
        window.location.replace(a);
    };

}]);