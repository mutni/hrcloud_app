﻿myApp.controller('emailsController', ['$scope', '$routeParams', 'emailsService', '$location', function ($scope, $routeParams, emailsService, $location) {

    $scope.getEmails = function (id) {
        emailsService.getEmails(id)
            .success(function (data) {
                $scope.emails = data;
                $scope.idContact = $routeParams.idContact;
                $scope.name = $routeParams.name;

            })
            .error(function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
                console.log($scope.status);
            });
    }
    if ($routeParams.idContact != "" && $routeParams.idContact != null) {
        $scope.getEmails($routeParams.idContact);
    }
    if ($routeParams.query != "" && $routeParams.query != null) {
        $scope.query = $routeParams.query;
    }

    $scope.delete = function (idEmail) {
        emailsService.delete(idEmail)
            .success(function (data) {
                if (data == 'True') {
                    $scope.getEmails($routeParams.idContact);
                }
                else {
                    confirm("Cannot delete email. At least one email is required!");
                }
            })
    };

    if ($routeParams.idContact != "" && $routeParams.idContact != null & $routeParams.name != "" && $routeParams.name != null) {
        $scope.idContact = $routeParams.idContact;
        $scope.name = $routeParams.name;
    }

    $scope.addEmail = function (idContact, email, primary) {
        emailsService.addEmail(idContact, email, primary)
            .success(function (data) {
                if (data == 'True') {
                    $location.url('/emails/' + $routeParams.idContact + '/' + $routeParams.name);
                }
                else
                    confirm("Invalid email!");
            })
    };

    $scope.change = function (emailId) {
        emailsService.change(emailId)
            .success(function (data) {
                if (data == 'True') {
                    $scope.getEmails($routeParams.idContact);
                }
                else
                    confirm("Cannot change to primary number!");
            })
    };

    $scope.action = function (location) {
        var a = '#/search/' + location
        window.location.replace(a);
    };

}]);