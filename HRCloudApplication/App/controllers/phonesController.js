﻿myApp.controller('phonesController', ['$scope', '$routeParams', 'phonesService', '$location', function ($scope, $routeParams, phonesService, $location) {

    $scope.getPhones = function (id) {
        phonesService.getPhones(id)
            .success(function (data) {
                $scope.phones = data;
                $scope.idContact = $routeParams.idContact;
                $scope.name = $routeParams.name;

            })
            .error(function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
                console.log($scope.status);
            });
    }
    if ($routeParams.idContact != "" && $routeParams.idContact != null) {
        $scope.getPhones($routeParams.idContact);
    }
    if ($routeParams.query != "" && $routeParams.query != null) {
        $scope.query = $routeParams.query;
    }

    $scope.delete = function (idPhone) {
        phonesService.delete(idPhone)
            .success(function (data) {
                if (data == 'True') {
                    $scope.getPhones($routeParams.idContact);
                }
                else {
                    confirm("Cannot delete phone. At least one phone is required!");
                }
            })
    };

    if ($routeParams.idContact != "" && $routeParams.idContact != null & $routeParams.name != "" && $routeParams.name != null) {
        $scope.idContact = $routeParams.idContact;
        $scope.name = $routeParams.name;
    }

    $scope.addPhone = function (idContact, phone, primary) {
        phonesService.addPhone(idContact, phone, primary)
            .success(function (data) {
                if (data == 'True') {
                    $location.url('/phones/' + $routeParams.idContact + '/' + $routeParams.name);
                }
                else
                    confirm("Invalid phone number!");
            })
    };


    $scope.change = function (phoneId) {
        phonesService.change(phoneId)
            .success(function (data) {
                if (data == 'True') {
                    $scope.getPhones($routeParams.idContact);
                }
                else
                    confirm("Cannot change to primary number!");
            })
    };

    $scope.action = function (location) {
        var a = '#/search/' + location
        window.location.replace(a);
    };

}]);