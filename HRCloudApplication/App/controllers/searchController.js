﻿myApp.controller('searchController', ['$scope', '$routeParams', 'searchService', '$location', function ($scope, $routeParams, searchService, $location) {
    
    searchService.getContacts(window.decodeURIComponent($routeParams.query))
        .success(function (data) {
            $scope.contacts = data;
            $scope.query = $routeParams.query;
            $scope.search = "";
        })

    $scope.action = function (location) {
        var a = '#/search/' + location
        window.location.replace(a);
    };
}]);