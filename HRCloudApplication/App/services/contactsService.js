﻿myApp.factory('contactsService', ['$http', function ($http) {
    var contactsService = {};

    contactsService.getContacts = function () {
        return $http.get('/Contacts/Index/');
    }

    contactsService.getDetails = function (param) {
        return $http.get('/Contacts/Details/' + param);
    }

    contactsService.create = function (name, surname, address, email, phone, tags) {
        return $http.get('/Contacts/Create/?Name=' + name + '&Surname=' + surname + '&Address=' + address + '&Email=' + email + '&Number=' + phone + '&Tags=' + tags)
    }

    contactsService.edit = function (id, name, surname, address) {
        return $http.get('/Contacts/Edit/?IdContact=' + id + '&Name=' + name + '&Surname=' + surname + '&Address=' + address)
    }

    contactsService.delete = function (id) {
        return $http.get('/Contacts/Delete/?id=' + id);
    }

    contactsService.getAllTags = function () {
        return $http.get('/Tags/AllTags/');
    }

    return contactsService;
}]);