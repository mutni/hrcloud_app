﻿myApp.factory('tagsService', ['$http', function ($http) {
    var tagsService = {};

    tagsService.getTags = function (id) {
        return $http.get('/Tags/Index/' + id);
    }

    tagsService.addTag = function (id, tag) {
        return $http.get('/Tags/Create/?Tags=' + tag + '&IdContact=' + id)
    }

    tagsService.delete = function (idTag, idContact) {
        return $http.get('/Tags/Delete/?idTag=' + idTag + '&idContact=' + idContact);
    }


    return tagsService;
}]);