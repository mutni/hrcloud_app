﻿myApp.factory('searchService', ['$http', function ($http) {
    var searchService = {};

    searchService.getContacts = function (query) {
        return $http.get('/Contacts/Index/?query=' + query);
    }

    return searchService;
}]);