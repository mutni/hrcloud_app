﻿myApp.factory('phonesService', ['$http', function ($http) {
    var phonesService = {};

    phonesService.getPhones = function (id) {
        return $http.get('/Phones/Index/' + id);
    }

    phonesService.addPhone = function (id, phone, primary) {
        return $http.get('/Phones/Create/?Number=' + phone + '&IdContact=' + id + '&IsPrimary=' + primary)
    }

    phonesService.delete = function (idPhone) {
        return $http.get('/Phones/Delete/' + idPhone);
    }

    phonesService.change = function (idPhone) {
        return $http.get('/Phones/Primary/' + idPhone);
    }


    return phonesService;
}]);