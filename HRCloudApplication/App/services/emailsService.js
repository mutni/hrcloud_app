﻿myApp.factory('emailsService', ['$http', function ($http) {
    var emailsService = {};

    emailsService.getEmails = function (id) {
        return $http.get('/Emails/Index/' + id);
    }

    emailsService.addEmail = function (id, email, primary) {
        return $http.get('/Emails/Create/?Name=' + email + '&IdContact=' + id + '&IsPrimary=' + primary)
    }
    
    emailsService.delete = function (idEmail) {
        return $http.get('/Emails/Delete/' + idEmail);
    }

    emailsService.change = function (idPhone) {
        return $http.get('/Emails/Primary/' + idPhone);
    }

    return emailsService;
}]);