﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRCloudApplication.Models.DAL
{
    public class InitialDatabase : System.Data.Entity.DropCreateDatabaseAlways<ContactContext>
    {
        protected override void Seed(ContactContext context)
        {
            var tags = new List<Tag>
            {
                new Tag{TagName="PHP"},
                new Tag{TagName="Java"},
                new Tag{TagName="JavaScript"},
                new Tag{TagName="HTML"},
                new Tag{TagName="CSS"},
                new Tag{TagName="SQL"},
                new Tag{TagName="admin"},
            };
            tags.ForEach(s => context.Tags.Add(s));
            context.SaveChanges();


            var contacts = new List<Contact>();
            Contact c = new Contact { Name = "Ivan", Surname = "Petrović", Address = "Savska 15, Zagreb" };
            List<Email> lstEmails = new List<Email>
            {
                new Email{Name="ivan@gmail.com", IsPrimary=true},
                new Email{Name="ivan2@gmail.com", IsPrimary=false}
            };
            c.Emails = lstEmails;
            List<Phone> lstPhones = new List<Phone>
            {
                new Phone{Number="0985273528", IsPrimary=true},
                new Phone{Number="0998263725", IsPrimary=false},
                new Phone{Number="0918253782", IsPrimary=false}
            };
            c.Phones = lstPhones;
            contacts.Add(c);

            c = new Contact { Name = "Marko", Surname = "Ivić", Address = "Jarunska 28, Zagreb" };
            lstEmails = new List<Email>
            {
                new Email{Name="marko@gmail.com", IsPrimary=false},
                new Email{Name="marko2@gmail.com", IsPrimary=true}
            };
            c.Emails = lstEmails;
            lstPhones = new List<Phone>
            {
                new Phone{Number="098183740", IsPrimary=true},
                new Phone{Number="0990384723", IsPrimary=false},
            };
            c.Phones = lstPhones;
            contacts.Add(c);

            c = new Contact { Name = "Josipa", Surname = "Marinović", Address = "Osječka 10, Split" };
            lstEmails = new List<Email>
            {
                new Email{Name="josipa@gmail.com", IsPrimary=true},
                new Email{Name="josipa2@gmail.com", IsPrimary=false},
                new Email{Name="josipa3@gmail.com", IsPrimary=false}
            };
            c.Emails = lstEmails;
            lstPhones = new List<Phone>
            {
                new Phone{Number="0986273827", IsPrimary=true},
                new Phone{Number="0919382748", IsPrimary=false},
            };
            c.Phones = lstPhones;
            contacts.Add(c);

            c = new Contact { Name = "Ivana", Surname = "Sarić", Address = "Crkvena 34, Osijek" };
            lstEmails = new List<Email>
            {
                new Email{Name="ivana@gmail.com", IsPrimary=true},
                new Email{Name="ivana2@gmail.com", IsPrimary=false},
            };
            c.Emails = lstEmails;
            lstPhones = new List<Phone>
            {
                new Phone{Number="0918374928", IsPrimary=true},
                new Phone{Number="0998473462", IsPrimary=false},
            };
            c.Phones = lstPhones;
            contacts.Add(c);
            contacts.ForEach(s => context.Contacts.Add(s));
            context.SaveChanges();


            var contactTags = new List<ContactTags>
            {
                new ContactTags{ContactId=1, TagId=1},
                new ContactTags{ContactId=1, TagId=4},
                new ContactTags{ContactId=1, TagId=5},
                new ContactTags{ContactId=1, TagId=6},
                new ContactTags{ContactId=2, TagId=2},
                new ContactTags{ContactId=2, TagId=4},
                new ContactTags{ContactId=2, TagId=5},
                new ContactTags{ContactId=2, TagId=7},
                new ContactTags{ContactId=3, TagId=3},
                new ContactTags{ContactId=3, TagId=4},
                new ContactTags{ContactId=3, TagId=5},
                new ContactTags{ContactId=3, TagId=6},
                new ContactTags{ContactId=4, TagId=1},
                new ContactTags{ContactId=4, TagId=2},
                new ContactTags{ContactId=4, TagId=6},
            };
            contactTags.ForEach(s => context.ContactTags.Add(s));
            context.SaveChanges();
        }
    }
}