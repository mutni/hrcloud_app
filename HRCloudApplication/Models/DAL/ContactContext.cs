﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HRCloudApplication.Models
{
    public class ContactContext : DbContext
    {
        public ContactContext()
            : base("name=connstring")
        {
        }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<ContactTags> ContactTags { get; set; }
    }
}

