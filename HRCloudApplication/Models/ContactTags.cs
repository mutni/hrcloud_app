﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRCloudApplication.Models
{
    public class ContactTags
    {
        public int id { get; set; }
        public int ContactId { get; set; }
        public int TagId { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual Tag Tag { get; set; }
    }
}