﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRCloudApplication.Models
{
    public class EmailAddModel
    {
        public int IdEmail { get; set; }
        public int IdContact { get; set; }
        [EmailAddress(ErrorMessage = "Invalid email!")]
        [Display(Name = "Email")]
        public string Name { get; set; }
        public bool IsPrimary { get; set; }
    }
}