﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HRCloudApplication.Models
{
    public class Email
    {
        [Key]
        public int IdEmail { get; set; }

        [EmailAddress(ErrorMessage = "Invalid email!")]
        [DataType(DataType.EmailAddress)]
        [Required]
        [Display(Name = "Email")]
        public string Name { get; set; }

        public bool IsPrimary { get; set; }

        public int ContactId { get; set; }
        [ForeignKey("ContactId")]
        public virtual Contact Contact { get; set; }
    }
}