﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HRCloudApplication.Models
{
    public class Tag
    {
        [Key]
        public int IdTag { get; set; }

        public string TagName { get; set; }

        //public int ContactId { get; set; }
        //[ForeignKey("ContactId")]

        public virtual List<ContactTags> Tags { get; set; }
    }
}