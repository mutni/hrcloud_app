﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRCloudApplication.Models
{
    public class TagAddModel
    {
        public int IdTag { get; set; }
        public int IdContact { get; set; }
        public string Name { get; set; }
    }
}