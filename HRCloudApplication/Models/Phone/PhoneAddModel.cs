﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRCloudApplication.Models
{
    public class PhoneAddModel
    {
        public int IdPhone { get; set; }
        public int IdContact { get; set; }
        public string Number { get; set; }
        public bool IsPrimary { get; set; }
    }
}