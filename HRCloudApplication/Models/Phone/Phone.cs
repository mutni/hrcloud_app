﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HRCloudApplication.Models
{
    public class Phone
    {
        [Key]
        public int IdPhone { get; set; }

        [Display(Name = "Phone number")]
        public string Number { get; set; }

        public bool IsPrimary { get; set; }

        public int ContactId { get; set; }
        [ForeignKey("ContactId")]
        public virtual Contact Contact { get; set; }
    }
}