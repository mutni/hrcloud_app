﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRCloudApplication.Models
{
    public class CreateContactViewModel
    {
        [Required]
        [Display(Name = "First name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string Surname { get; set; }

        [Required]
        public string Address { get; set; }

        [EmailAddress(ErrorMessage = "Invalid email!")]
        [DataType(DataType.EmailAddress)]
        [Required]
        public string Email { get; set; }

        [Display(Name = "Phone number")]
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string Number { get; set; }

        public string Tags { get; set; }
    }
}