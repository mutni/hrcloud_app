﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRCloudApplication.Models
{
    public class Contact
    {
        [Key]
        public int IdContact { get; set; }

        [Required]
        [Display(Name = "First name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string Surname { get; set; }

        [Required]
        public string Address { get; set; }

        public virtual List<Email> Emails { get; set; }
        public virtual List<Phone> Phones { get; set; }
        public virtual List<ContactTags> Tags { get; set; }

        public Contact()
        {
            Emails = new List<Email>();
            Phones = new List<Phone>();
        }

    }
}