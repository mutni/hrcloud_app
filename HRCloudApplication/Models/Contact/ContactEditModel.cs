﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRCloudApplication.Models
{
    public class ContactEditModel
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public List<string> Emails { get; set; }
        [Display(Name = "Phone numbers")]
        public List<string> Phones { get; set; }
        public List<string> Tags { get; set; }
    }
}