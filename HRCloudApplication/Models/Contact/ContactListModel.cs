﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRCloudApplication.Models
{
    public class ContactListModel
    {
        [Key]
        public int IdContact { get; set; }

        [Display(Name = "First name")]
        public string Name { get; set; }

        [Display(Name = "Last name")]
        public string Surname { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public List<string> Tags { get; set; }
    }
}