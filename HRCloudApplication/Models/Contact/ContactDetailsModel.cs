﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRCloudApplication.Models
{
    public class ContactDetailsModel
    {
        public int IdContact { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public List<string> Emails { get; set; }
        public List<string> Phones { get; set; }
        public List<string> Tags { get; set; }
    }
}