﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HRCloudApplication.Models;
using Newtonsoft.Json;
using System.Diagnostics;

namespace HRCloudApplication.Controllers
{
    public class ContactsController : Controller
    {
        private ContactContext db = new ContactContext();

        // GET: Contacts
        public ActionResult Index()
        {
            string query = "";
            query = Request.QueryString["query"];
            List<Contact> lstContacts = new List<Contact>();
            if (query != null && query != "")
            {
                query = query.Replace("%20", "");
                string[] tags = query.Split(',');
                foreach (string tag in tags)
                {
                    List<Contact> lstHits = new List<Contact>();

                    //lstHits = (from c in db.Contacts
                    //           join t in db.Tags on c.IdContact equals t.ContactId
                    //           where c.Name.ToLower() == tag.Trim().ToLower() || c.Surname.ToLower() == tag.Trim().ToLower() || t.TagName.Trim().ToLower() == tag.Trim().ToLower()
                    //           select c).ToList();

                    lstHits = (from c in db.Contacts
                               join ct in db.ContactTags on c.IdContact equals ct.ContactId
                               join t in db.Tags on ct.TagId equals t.IdTag
                               where c.Name.ToLower().Contains(tag.Trim().ToLower()) || c.Surname.ToLower().Contains(tag.Trim().ToLower()) || t.TagName.Trim().ToLower().Contains(tag.Trim().ToLower())
                               select c).ToList();
                    foreach (Contact item in lstHits)
                    {
                        if (!lstContacts.Contains(item))
                        {
                            lstContacts.Add(item);
                        }
                    }

                }
            }
            else
            {
                lstContacts = db.Contacts.ToList();
            }
            List<ContactListModel> lstModel = new List<ContactListModel>();
            foreach (Contact item in lstContacts)
            {
                Email email = (from e in item.Emails
                               where e.IsPrimary == true
                               select e).First();

                Phone phone = (from e in item.Phones
                               where e.IsPrimary == true
                               select e).First();
                List<string> lstTags = (from t in db.Tags
                                        join ct in db.ContactTags on t.IdTag equals ct.TagId
                                        where ct.ContactId == item.IdContact
                                        select t.TagName).ToList();
                //foreach (Tag tag in item.Tags)
                //{
                //    lstTags.Add(tag.TagName);
                //}

                ContactListModel c = new ContactListModel()
                {
                    IdContact = item.IdContact,
                    Name = item.Name,
                    Surname = item.Surname,
                    Address = item.Address,
                    Email = email.Name,
                    Phone = phone.Number,
                    Tags = lstTags
                };
                lstModel.Add(c);
            }
            return Json(lstModel, JsonRequestBehavior.AllowGet);
            //return View(lstModel);
        }

        // GET: Contacts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            ContactDetailsModel model = new ContactDetailsModel()
            {
                IdContact = contact.IdContact,
                Name = contact.Name,
                Surname = contact.Surname,
                Address = contact.Address
            };

            model.Emails = new List<string>();
            foreach (Email item in contact.Emails)
            {
                model.Emails.Add(item.Name);
            }

            model.Tags = (from t in db.Tags
                          join ct in db.ContactTags on t.IdTag equals ct.TagId
                          where ct.ContactId == contact.IdContact
                          select t.TagName).ToList();

            model.Phones = new List<string>();
            foreach (Phone item in contact.Phones)
            {
                model.Phones.Add(item.Number);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
            //return View(model);
        }

        public bool Create(CreateContactViewModel model)
        {
            if (ModelState.IsValid)
            {
                Contact contact = new Contact()
                {
                    Name = model.Name,
                    Surname = model.Surname,
                    Address = model.Address,
                };

                Email email = new Email()
                {
                    Name = model.Email,
                    IsPrimary = true
                };
                contact.Emails.Add(email);

                Phone phone = new Phone()
                {
                    Number = model.Number,
                    IsPrimary = true
                };
                contact.Phones.Add(phone);

                db.Contacts.Add(contact);
                db.SaveChanges();

                string[] tags = model.Tags.Split(',');
                foreach (string item in tags)
                {
                    Tag tag = db.Tags.Where(t => t.TagName == item.Trim()).FirstOrDefault();
                    if (tag == null)
                    {
                        tag = new Tag()
                        {
                            TagName = item.Trim()
                        };
                        db.Tags.Add(tag);
                        db.SaveChanges();
                    }
                    db.ContactTags.Add(new ContactTags() { ContactId = contact.IdContact, TagId = tag.IdTag });
                    db.SaveChanges();
                }

                return true;

            }

            return false;
        }

        public bool Edit([Bind(Include = "IdContact,Name,Surname,Address")] Contact contact)
        {
            if (ModelState.IsValid)
            {

                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Delete(int id)
        {
            Contact contact = db.Contacts.Find(id);

            //foreach (Tag item in contact.Tags.ToList())
            //{
            //    Tag tag = db.Tags.Find(item.IdTag);
            //    db.Tags.Remove(tag);
            //}

            foreach (Email item in contact.Emails.ToList())
            {
                Email email = db.Emails.Find(item.IdEmail);
                db.Emails.Remove(email);
            }

            foreach (Phone item in contact.Phones.ToList())
            {
                Phone phone = db.Phones.Find(item.IdPhone);
                db.Phones.Remove(phone);
            }

            db.Contacts.Remove(contact);
            db.SaveChanges();
            return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
