﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HRCloudApplication.Models;

namespace HRCloudApplication.Controllers
{
    public class EmailsController : Controller
    {
        private ContactContext db = new ContactContext();

        // GET: Emails
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }

            List<EmailAddModel> Emails = new List<EmailAddModel>();
            foreach (Email item in contact.Emails)
            {
                EmailAddModel model = new EmailAddModel()
                {
                    IdContact = contact.IdContact,
                    IdEmail = item.IdEmail,
                    Name = item.Name    ,
                    IsPrimary = item.IsPrimary
                };
                Emails.Add(model);
            }
            return Json(Emails, JsonRequestBehavior.AllowGet);
        }
  
        public bool Create(EmailAddModel model)
        {
            if (ModelState.IsValid)
            {
                if(model.IsPrimary == true)
                {
                    Contact c = db.Contacts.Find(model.IdContact);
                    Email e = (from em in c.Emails
                               where em.IsPrimary == true
                               select em).First();
                    e.IsPrimary = false;
                    db.Entry(e).State = EntityState.Modified;
                    db.SaveChanges();
                }
                Email email = new Email()
                {
                    Name = model.Name,
                    ContactId = model.IdContact,
                    IsPrimary = model.IsPrimary
                };
                db.Emails.Add(email);
                db.SaveChanges();
                return true;
            }

            return false;
        }
  
        public bool Delete(int id)
        {
            Email email = db.Emails.Find(id);
            Contact contact = db.Contacts.Find(email.ContactId);
            
            if(contact.Emails.Count() > 1 && email.IsPrimary != true)
            {
                db.Emails.Remove(email);
                db.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Primary(int id)
        {
            try
            {
                Email email = db.Emails.Find(id);
                Contact c = db.Contacts.Find(email.ContactId);

                Email p = (from ph in c.Emails
                           where ph.IsPrimary == true
                           select ph).First();

                p.IsPrimary = false;
                db.Entry(p).State = EntityState.Modified;
                db.SaveChanges();

                email.IsPrimary = true;
                db.Entry(p).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
