﻿using HRCloudApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRCloudApplication.Controllers
{
    public class HomeController : Controller
    {
        private ContactContext db = new ContactContext();
        public ActionResult Index()
        {
            return View();
        }
    }
}