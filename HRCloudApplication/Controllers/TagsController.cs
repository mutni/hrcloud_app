﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HRCloudApplication.Models;
using System.Diagnostics;

namespace HRCloudApplication.Controllers
{
    public class TagsController : Controller
    {
        private ContactContext db = new ContactContext();

        public ActionResult AllTags()
        {
            List<string> lstTags = (from t in db.Tags
                                    select t.TagName).ToList();
            return Json(lstTags, JsonRequestBehavior.AllowGet);
        }


        // GET: Tags
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }

            List<TagAddModel> Tags = new List<TagAddModel>();
            var lstTags = (from t in db.Tags
                           join ct in db.ContactTags on t.IdTag equals ct.TagId
                           where ct.ContactId == contact.IdContact
                           select new { t.TagName, t.IdTag }).ToList();

            foreach (var item in lstTags)
            {
                TagAddModel model = new TagAddModel()
                {
                    IdContact = contact.IdContact,
                    IdTag = item.IdTag,
                    Name = item.TagName
                };
                Tags.Add(model);
            }
            return Json(Tags, JsonRequestBehavior.AllowGet);
        }

        public bool Create(string Tags, int? IdContact)
        {
            if(!String.IsNullOrEmpty(Tags) && IdContact != null)
            {
                string[] tags = Tags.Split(',');
                foreach (string item in tags)
                {
                    Tag tag = db.Tags.Where(t => t.TagName == item.Trim()).FirstOrDefault();
                    if (tag == null)
                    {
                        tag = new Tag()
                        {
                            TagName = item.Trim()
                        };
                        db.Tags.Add(tag);
                        db.SaveChanges();
                    }
                    db.ContactTags.Add(new ContactTags() { ContactId = (int)IdContact, TagId = tag.IdTag });
                    db.SaveChanges();
                }
                return true;
            }

            return false;
        }

        public bool Delete(int idTag, int idContact)
        {
            ContactTags ct = db.ContactTags.Where(t => t.TagId == idTag && t.ContactId == idContact).First();
            db.ContactTags.Remove(ct);
            db.SaveChanges();

            ct = db.ContactTags.Where(t => t.TagId == idTag).FirstOrDefault();
            if(ct == null)
            {
                Tag t = db.Tags.Find(idTag);
                db.Tags.Remove(t);
                db.SaveChanges();
            }

            return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
