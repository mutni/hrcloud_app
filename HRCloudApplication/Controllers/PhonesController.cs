﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HRCloudApplication.Models;

namespace HRCloudApplication.Controllers
{
    public class PhonesController : Controller
    {
        private ContactContext db = new ContactContext();

        // GET: Phones
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }

            List<PhoneAddModel> Phones = new List<PhoneAddModel>();
            foreach (Phone item in contact.Phones)
            {
                PhoneAddModel model = new PhoneAddModel()
                {
                    IdContact = contact.IdContact,
                    IdPhone = item.IdPhone,
                    Number = item.Number,
                    IsPrimary = item.IsPrimary
                };
                Phones.Add(model);
            }
            return Json(Phones, JsonRequestBehavior.AllowGet);
        }

        public bool Create(PhoneAddModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.IsPrimary == true)
                {
                    Contact c = db.Contacts.Find(model.IdContact);
                    Phone p = (from ph in c.Phones
                               where ph.IsPrimary == true
                               select ph).First();
                    p.IsPrimary = false;
                    db.Entry(p).State = EntityState.Modified;
                    db.SaveChanges();
                }

                Phone phone = new Phone()
                {
                    Number = model.Number,
                    ContactId = model.IdContact,
                    IsPrimary = model.IsPrimary
                };
                db.Phones.Add(phone);
                db.SaveChanges();
                return true;
            }

            return false;
        }

        public bool Delete(int id)
        {
            Phone phone = db.Phones.Find(id);
            Contact contact = db.Contacts.Find(phone.ContactId);

            if (contact.Phones.Count() > 1 && phone.IsPrimary != true)
            {
                db.Phones.Remove(phone);
                db.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Primary(int id)
        {
            try
            {
                Phone phone = db.Phones.Find(id);
                Contact c = db.Contacts.Find(phone.ContactId);

                Phone p = (from ph in c.Phones
                           where ph.IsPrimary == true
                           select ph).First();

                p.IsPrimary = false;
                db.Entry(p).State = EntityState.Modified;
                db.SaveChanges();

                phone.IsPrimary = true;
                db.Entry(p).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
