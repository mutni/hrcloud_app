namespace HRCloudApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class isPrimary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Emails", "IsPrimary", c => c.Boolean(nullable: false));
            AddColumn("dbo.Phones", "IsPrimary", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Phones", "IsPrimary");
            DropColumn("dbo.Emails", "IsPrimary");
        }
    }
}
