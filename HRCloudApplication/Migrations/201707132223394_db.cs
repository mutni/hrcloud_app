namespace HRCloudApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class db : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Emails", "Contact_IdContact", "dbo.Contacts");
            DropIndex("dbo.Emails", new[] { "Contact_IdContact" });
            DropColumn("dbo.Emails", "ContactId");
            RenameColumn(table: "dbo.Emails", name: "Contact_IdContact", newName: "ContactId");
            AlterColumn("dbo.Emails", "ContactId", c => c.Int(nullable: false));
            CreateIndex("dbo.Emails", "ContactId");
            AddForeignKey("dbo.Emails", "ContactId", "dbo.Contacts", "IdContact", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Emails", "ContactId", "dbo.Contacts");
            DropIndex("dbo.Emails", new[] { "ContactId" });
            AlterColumn("dbo.Emails", "ContactId", c => c.Int());
            RenameColumn(table: "dbo.Emails", name: "ContactId", newName: "Contact_IdContact");
            AddColumn("dbo.Emails", "ContactId", c => c.Int(nullable: false));
            CreateIndex("dbo.Emails", "Contact_IdContact");
            AddForeignKey("dbo.Emails", "Contact_IdContact", "dbo.Contacts", "IdContact");
        }
    }
}
