namespace HRCloudApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tblContactTags : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Tags", new[] { "Contact_IdContact" });
            CreateTable(
                "dbo.ContactTags",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        ContactId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                        Contact_IdContact = c.Int(),
                        Tag_IdTag = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Tags", t => t.Tag_IdTag)
                .Index(t => t.Contact_IdContact)
                .Index(t => t.Tag_IdTag);
            
            DropColumn("dbo.Tags", "Contact_IdContact");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tags", "Contact_IdContact", c => c.Int());
            DropForeignKey("dbo.ContactTags", "Tag_IdTag", "dbo.Tags");
            DropIndex("dbo.ContactTags", new[] { "Tag_IdTag" });
            DropIndex("dbo.ContactTags", new[] { "Contact_IdContact" });
            DropTable("dbo.ContactTags");
            CreateIndex("dbo.Tags", "Contact_IdContact");
        }
    }
}
