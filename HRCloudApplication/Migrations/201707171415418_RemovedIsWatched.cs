namespace HRCloudApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedIsWatched : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Contacts", "IsWatched");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contacts", "IsWatched", c => c.Boolean(nullable: false));
        }
    }
}
