namespace HRCloudApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PhoneFK : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Phones", "Contact_IdContact", "dbo.Contacts");
            DropIndex("dbo.Phones", new[] { "Contact_IdContact" });
            DropColumn("dbo.Phones", "ContactId");
            RenameColumn(table: "dbo.Phones", name: "Contact_IdContact", newName: "ContactId");
            AlterColumn("dbo.Phones", "ContactId", c => c.Int(nullable: false));
            CreateIndex("dbo.Phones", "ContactId");
            AddForeignKey("dbo.Phones", "ContactId", "dbo.Contacts", "IdContact", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Phones", "ContactId", "dbo.Contacts");
            DropIndex("dbo.Phones", new[] { "ContactId" });
            AlterColumn("dbo.Phones", "ContactId", c => c.Int());
            RenameColumn(table: "dbo.Phones", name: "ContactId", newName: "Contact_IdContact");
            AddColumn("dbo.Phones", "ContactId", c => c.Int(nullable: false));
            CreateIndex("dbo.Phones", "Contact_IdContact");
            AddForeignKey("dbo.Phones", "Contact_IdContact", "dbo.Contacts", "IdContact");
        }
    }
}
