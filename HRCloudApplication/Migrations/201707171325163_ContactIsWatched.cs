namespace HRCloudApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContactIsWatched : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contacts", "IsWatched", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contacts", "IsWatched");
        }
    }
}
