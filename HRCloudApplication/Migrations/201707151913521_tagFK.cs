namespace HRCloudApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tagFK : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tags", "Contact_IdContact", "dbo.Contacts");
            DropIndex("dbo.Tags", new[] { "Contact_IdContact" });
            RenameColumn(table: "dbo.Tags", name: "Contact_IdContact", newName: "ContactId");
            AlterColumn("dbo.Tags", "ContactId", c => c.Int(nullable: false));
            CreateIndex("dbo.Tags", "ContactId");
            AddForeignKey("dbo.Tags", "ContactId", "dbo.Contacts", "IdContact", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tags", "ContactId", "dbo.Contacts");
            DropIndex("dbo.Tags", new[] { "ContactId" });
            AlterColumn("dbo.Tags", "ContactId", c => c.Int());
            RenameColumn(table: "dbo.Tags", name: "ContactId", newName: "Contact_IdContact");
            CreateIndex("dbo.Tags", "Contact_IdContact");
            AddForeignKey("dbo.Tags", "Contact_IdContact", "dbo.Contacts", "IdContact");
        }
    }
}
