namespace HRCloudApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newTagModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tags", "ContactId", "dbo.Contacts");
            DropIndex("dbo.Tags", new[] { "ContactId" });
            RenameColumn(table: "dbo.Tags", name: "ContactId", newName: "Contact_IdContact");
            AlterColumn("dbo.Tags", "Contact_IdContact", c => c.Int());
            CreateIndex("dbo.Tags", "Contact_IdContact");
            AddForeignKey("dbo.Tags", "Contact_IdContact", "dbo.Contacts", "IdContact");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tags", "Contact_IdContact", "dbo.Contacts");
            DropIndex("dbo.Tags", new[] { "Contact_IdContact" });
            AlterColumn("dbo.Tags", "Contact_IdContact", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Tags", name: "Contact_IdContact", newName: "ContactId");
            CreateIndex("dbo.Tags", "ContactId");
            AddForeignKey("dbo.Tags", "ContactId", "dbo.Contacts", "IdContact", cascadeDelete: true);
        }
    }
}
