namespace HRCloudApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        IdContact = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Surname = c.String(nullable: false),
                        Address = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdContact);
            
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        IdEmail = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        ContactId = c.Int(nullable: false),
                        Contact_IdContact = c.Int(),
                    })
                .PrimaryKey(t => t.IdEmail)
                .ForeignKey("dbo.Contacts", t => t.Contact_IdContact)
                .Index(t => t.Contact_IdContact);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Emails", "Contact_IdContact", "dbo.Contacts");
            DropIndex("dbo.Emails", new[] { "Contact_IdContact" });
            DropTable("dbo.Emails");
            DropTable("dbo.Contacts");
        }
    }
}
