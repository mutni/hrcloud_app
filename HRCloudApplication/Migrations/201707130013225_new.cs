namespace HRCloudApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _new : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Phones",
                c => new
                    {
                        IdPhone = c.Int(nullable: false, identity: true),
                        Number = c.String(),
                        ContactId = c.Int(nullable: false),
                        Contact_IdContact = c.Int(),
                    })
                .PrimaryKey(t => t.IdPhone)
                .ForeignKey("dbo.Contacts", t => t.Contact_IdContact)
                .Index(t => t.Contact_IdContact);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        IdTag = c.Int(nullable: false, identity: true),
                        TagName = c.String(),
                        Contact_IdContact = c.Int(),
                    })
                .PrimaryKey(t => t.IdTag)
                .ForeignKey("dbo.Contacts", t => t.Contact_IdContact)
                .Index(t => t.Contact_IdContact);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tags", "Contact_IdContact", "dbo.Contacts");
            DropForeignKey("dbo.Phones", "Contact_IdContact", "dbo.Contacts");
            DropIndex("dbo.Tags", new[] { "Contact_IdContact" });
            DropIndex("dbo.Phones", new[] { "Contact_IdContact" });
            DropTable("dbo.Tags");
            DropTable("dbo.Phones");
        }
    }
}
